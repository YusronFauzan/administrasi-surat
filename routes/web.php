<?php

use Illuminate\Support\Facades\Route;
use App\Http\Controllers;
use App\Http\Controllers\DashboardController;
use App\Http\Controllers\PengajuanSuratController;
use App\Http\Controllers\SuratController;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::middleware('auth')->group(function(){
    // User
    Route::get('/', [DashboardController::class, 'index'])->name('dashboard');
    Route::get('/dashboard', [DashboardController::class, 'index'])->name('dashboard');
    Route::get('/pengajuan-surat', [PengajuanSuratController::class, 'index'])->name('surat.index');
    Route::post('/pengajuan-surat/store', [PengajuanSuratController::class, 'store'])->name('surat.store');
    // Admin
    Route::get('/pengajuan-surat/admin', [PengajuanSuratController::class, 'indexAdmin'])->name('surat.index.admin');
    Route::get('/pengajuan-surat/edit/{id}', [PengajuanSuratController::class, 'editAdmin'])->name('surat.admin.edit');
    Route::put('/pengajuan-surat/update', [PengajuanSuratController::class, 'updateAdmin'])->name('surat.admin.update');

    // Download Surat
    Route::get('/download-surat/{file}', [PengajuanSuratController::class, 'download'])->name('download.surat');

});

// Route::get('/dashboard', function () {
//     return view('dashboard');
// })->middleware(['auth'])->name('dashboard');

require __DIR__.'/auth.php';
