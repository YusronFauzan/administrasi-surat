<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class DataPemohon extends Model
{
    use HasFactory;
    public function permohonan(){
        return $this->hasMany(Permohonan::class);
    }
}
