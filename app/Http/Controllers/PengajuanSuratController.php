<?php

namespace App\Http\Controllers;

use App\Models\Permohonan;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Storage;

class PengajuanSuratController extends Controller
{
    public function index(){
        return view('pages.pengajuan-surat.pengajuan-surat', [
            'pengajuanSurat' => Permohonan::where('user_id', Auth::user()->id)->get()
        ]);
    }
    public function indexAdmin(){
        return view('pages.admin.pengajuan-surat', [
            'pengajuanSurat' => Permohonan::all()
        ]);
    }
    public function create(){
        
    }
    public function store(Request $request){
        $validatedData = $request->validate([
            'nama_mahasiswa' => 'required|string|max:255',
            'nim' => 'required|string|max:255',
            'program_studi' => 'required|string|max:255',
            'semester' => 'required|max:255',
            'tempat_lahir' => 'required|string|max:255',
            'tanggal_lahir' => 'date',
            'alamat' => 'required|string|max:255',
            'no_hp' => 'required|string|max:16',
            'jenis_surat' => 'required',
            'keperluan' => 'required|string|max:255'
        ]);
        $validatedData['user_id'] = Auth::user()->id;
        Permohonan::create($validatedData);

        return redirect()->route('surat.index');
    }
    public function editAdmin($id){
        return view('pages.admin.edit', [
            'pengajuanSurat' => Permohonan::findOrFail($id)
        ]);
    }
    public function download($file){
        $url = Storage::url($file);
        // return Storage::disk('local')->download($url);
    }
    public function updateAdmin(Request $request){
        $validatedData = $request->validate([
            'nama_mahasiswa' => 'required|string|max:255',
            'nim' => 'required|string|max:255',
            'program_studi' => 'required|string|max:255',
            'semester' => 'required|max:255',
            'tempat_lahir' => 'required|string|max:255',
            'tanggal_lahir' => 'date',
            'alamat' => 'required|string|max:255',
            'no_hp' => 'required|string|max:16',
            'keperluan' => 'required|string|max:255',
            'status' => 'max:1',
            'file' => 'mimes:doc,docx'
        ]);
        if ($request->file('file')) {
            $validatedData['file'] = $request->file('file')->store('surat-keterangan-aktif');
        }

        Permohonan::where('id', $request->id)->update($validatedData);

        return redirect()->route('surat.index.admin');
    }
}
