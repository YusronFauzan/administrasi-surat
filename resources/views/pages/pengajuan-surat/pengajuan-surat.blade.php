@extends('layouts.main')

@section('app.title', 'Pengajuan Surat')
@section('page.title', 'Pengajuan Surat')

@section('page.content')
    <div class="col-sm-12">
        <div class="row">
            <div class="col-sm-3 mb-3">
                <button type="button" class="btn btn-primary" data-bs-toggle="modal" data-bs-target="#pengajuanModal">
                    Buat Pengajuan Surat
                </button>
            </div>
            {{-- @if (session()->has('success'))
                <div class="alert alert-success" role="alert">
                    {{ session()->success}}
                </div>
            @endif --}}
        </div>
        <table class="table">
            <thead>
            <tr>
                <th scope="col">#</th>
                <th scope="col">Keperluan</th>
                <th scope="col">Jenis Keperluan</th>
                <th scope="col">Tanggal Pengajuan</th>
                <th scope="col">Keterangan</th>
                <th scope="col">Link</th>
            </tr>
            </thead>
            <tbody>
            @foreach ($pengajuanSurat as $ps)
                <tr>
                    <th>{{ $loop->iteration }}</th>
                    <td>{{ $ps->keperluan }}</td>
                    <td>{{ $ps->jenis_surat }}</td>
                    <td>{{ $ps->created_at }}</td>
                    @if ($ps->file)
                        <td>Selesai</td>
                    @else
                        <td>-</td>
                    @endif
                    @if ($ps->file)
                        <td><a href="{{asset('storage/'.$ps->file)}}" type="button">Download</a></td>
                    @else
                        <td>-</td>
                    @endif
                </tr>
            @endforeach
            </tbody>
        </table>
    
        <div class="modal fade" id="pengajuanModal" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel"
        aria-hidden="true">
            <div class="modal-dialog" role="document">
                <div class="modal-content">
                    <div class="modal-header">
                        <h5 class="modal-title" id="exampleModalLabel">Isi Data Diri</h5>
                        <button class="btn-close" type="button" data-bs-dismiss="modal" aria-label="Close">
                            <span aria-hidden="true"></span>
                        </button>
                    </div>
                    <div class="modal-body">
                        <form action="{{route('surat.store')}}" method="post" class="col-md-12">
                            @csrf
                            <div class="mb-3">
                                <label for="nama_mahasiswa" class="form-label">Nama Mahasiswa</label>
                                <input type="text" class="form-control @error('nama_mahasiswa') is-invalid @enderror" id="nama_mahasiswa" name="nama_mahasiswa">
                                @error('nama_mahasiswa')
                                    <div class="invalid-feedback">
                                        {{ $message }}
                                    </div>    
                                @enderror
                            </div>
                            <div class="mb-3">
                                <label for="nim" class="form-label">Nomor Induk Mahasiswa</label>
                                <input type="text" class="form-control @error('nim') is-invalid @enderror" id="nim" name="nim">
                                @error('nim')
                                    <div class="invalid-feedback">
                                        {{ $message }}
                                    </div>    
                                @enderror
                            </div>
                            <div class="mb-3">
                                <label for="program_studi" class="form-label">Program Studi</label>
                                <input type="text" class="form-control @error('program_studi') is-invalid @enderror" id="program_studi" name="program_studi">
                                @error('program_studi')
                                    <div class="invalid-feedback">
                                        {{ $message }}
                                    </div>    
                                @enderror
                            </div>
                            <div class="mb-3">
                                <label for="semester" class="form-label">Semester</label>
                                <input type="text" class="form-control @error('semester') is-invalid @enderror" id="semester" name="semester">
                                @error('semester')
                                    <div class="invalid-feedback">
                                        {{ $message }}
                                    </div>    
                                @enderror
                            </div>
                           
                            <div class="mb-3">
                                <label for="tempat_lahir" class="form-label">Tempat Lahir</label>
                                <input type="text" class="form-control @error('tempat_lahir') is-invalid @enderror" id="tempat_lahir" name="tempat_lahir">
                                @error('tempat_lahir')
                                    <div class="invalid-feedback">
                                        {{ $message }}
                                    </div>    
                                @enderror
                            </div>
                            <div class="mb-3">
                                <label for="tanggal_lahir" class="form-label">Tanggal Lahir</label>
                                <input type="date" class="form-control @error('tanggal_lahir') is-invalid @enderror" id="tanggal_lahir" name="tanggal_lahir">
                                @error('tanggal_lahir')
                                    <div class="invalid-feedback">
                                        {{ $message }}
                                    </div>    
                                @enderror
                            </div>
                            <div class="mb-3">
                                <label for="alamat" class="form-label">Alamat</label>
                                <input type="text" class="form-control @error('alamat') is-invalid @enderror" id="alamat" name="alamat">
                                @error('alamat')
                                    <div class="invalid-feedback">
                                        {{ $message }}
                                    </div>    
                                @enderror
                            </div>
                            <div class="mb-3">
                                <label for="no_hp" class="form-label">Nomor HP</label>
                                <input type="text" class="form-control @error('no_hp') is-invalid @enderror" id="no_hp" name="no_hp">
                                @error('no_hp')
                                    <div class="invalid-feedback">
                                        {{ $message }}
                                    </div>    
                                @enderror
                            </div>
                            <div class="mb-3">
                                <label for="jenis_surat" class="form-label">Jenis Surat</label>
                                <select class="form-select" name="jenis_surat" aria-label="Default select example">
                                    <option selected>Pilih Jenis Surat</option>
                                    <option value="Surat Keterangan Aktif">Surat Keterangan Aktif</option>
                                    <option value="Surat Permohonan Magang">Surat Permohonan Magang</option>
                                    <option value="Surat Pengantar Observasi">Surat Pengantar Observasi</option>
                                </select>
                                @error('keperluan')
                                    <div class="invalid-feedback">
                                        {{ $message }}
                                    </div>    
                                @enderror
                            </div>
                            <div class="mb-3">
                                <label for="keperluan" class="form-label">Keperluan Untuk</label>
                                <input type="text" class="form-control @error('keperluan') is-invalid @enderror" id="keperluan" name="keperluan">
                                @error('keperluan')
                                    <div class="invalid-feedback">
                                        {{ $message }}
                                    </div>    
                                @enderror
                            </div>
                    </div>
                    <div class="modal-footer">
                        <button class="btn btn-secondary" type="button" data-bs-dismiss="modal">Cancel</button>
                        <button class="btn btn-primary" type="submit">Buat</button>
                    </form>
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection