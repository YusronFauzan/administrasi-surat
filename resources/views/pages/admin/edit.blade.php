@extends('layouts.main')
@section('page.content')
    <div class="col-sm-12">
        <form action="{{route('surat.admin.update')}}" method="post" class="col-md-12" enctype="multipart/form-data">
            @csrf
            @method('put')
            <input type="hidden" name="id" value="{{$pengajuanSurat->id}}">
            <div class="mb-3">
                <label for="nama_mahasiswa" class="form-label">Nama Mahasiswa</label>
                <input type="text" class="form-control @error('nama_mahasiswa') is-invalid @enderror" id="nama_mahasiswa" name="nama_mahasiswa" value="{{$pengajuanSurat->nama_mahasiswa}}">
                @error('nama_mahasiswa')
                    <div class="invalid-feedback">
                        {{ $message }}
                    </div>    
                @enderror
            </div>
            <div class="mb-3">
                <label for="nim" class="form-label">Nomor Induk Mahasiswa</label>
                <input type="text" class="form-control @error('nim') is-invalid @enderror" id="nim" name="nim" value="{{$pengajuanSurat->nim}}">
                @error('nim')
                    <div class="invalid-feedback">
                        {{ $message }}
                    </div>    
                @enderror
            </div>
            <div class="mb-3">
                <label for="program_studi" class="form-label">Program Studi</label>
                <input type="text" class="form-control @error('program_studi') is-invalid @enderror" id="program_studi" name="program_studi" value="{{$pengajuanSurat->program_studi}}">
                @error('program_studi')
                    <div class="invalid-feedback">
                        {{ $message }}
                    </div>    
                @enderror
            </div>
            <div class="mb-3">
                <label for="semester" class="form-label">Semester</label>
                <input type="text" class="form-control @error('semester') is-invalid @enderror" id="semester" name="semester" value="{{$pengajuanSurat->semester}}">
                @error('semester')
                    <div class="invalid-feedback">
                        {{ $message }}
                    </div>    
                @enderror
            </div>
        
            <div class="mb-3">
                <label for="tempat_lahir" class="form-label">Tempat Lahir</label>
                <input type="text" class="form-control @error('tempat_lahir') is-invalid @enderror" id="tempat_lahir" name="tempat_lahir" value="{{$pengajuanSurat->tempat_lahir}}">
                @error('tempat_lahir')
                    <div class="invalid-feedback">
                        {{ $message }}
                    </div>    
                @enderror
            </div>
            <div class="mb-3">
                <label for="tanggal_lahir" class="form-label">Tanggal Lahir</label>
                <input type="date" class="form-control @error('tanggal_lahir') is-invalid @enderror" id="tanggal_lahir" name="tanggal_lahir" value="{{$pengajuanSurat->tanggal_lahir}}">
                @error('tanggal_lahir')
                    <div class="invalid-feedback">
                        {{ $message }}
                    </div>    
                @enderror
            </div>
            <div class="mb-3">
                <label for="alamat" class="form-label">Alamat</label>
                <input type="text" class="form-control @error('alamat') is-invalid @enderror" id="alamat" name="alamat" value="{{$pengajuanSurat->alamat}}">
                @error('alamat')
                    <div class="invalid-feedback">
                        {{ $message }}
                    </div>    
                @enderror
            </div>
            <div class="mb-3">
                <label for="no_hp" class="form-label">Nomor HP</label>
                <input type="text" class="form-control @error('no_hp') is-invalid @enderror" id="no_hp" name="no_hp" value="{{$pengajuanSurat->no_hp}}">
                @error('no_hp')
                    <div class="invalid-feedback">
                        {{ $message }}
                    </div>    
                @enderror
            </div>
            <div class="mb-3">
                <label for="keperluan" class="form-label">Keperluan Untuk</label>
                <input type="text" class="form-control @error('keperluan') is-invalid @enderror" id="keperluan" name="keperluan" value="{{$pengajuanSurat->keperluan}}">
                @error('keperluan')
                    <div class="invalid-feedback">
                        {{ $message }}
                    </div>    
                @enderror
            </div>
            <div class="mb-3">
                <label for="status" class="form-label">Status</label>
                <select class="form-select" aria-label="Default select example" id="status" name="status">
                    <option value="0" selected>0</option>
                    <option value="1">1</option>
                  </select>
                @error('status')
                    <div class="invalid-feedback">
                        {{ $message }}
                    </div>    
                @enderror
            </div>
            <div class="mb-3">
                <label for="formFile" class="form-label">Upload Surat</label>
                <input class="form-control" type="file" id="file" name="file">
              </div>
            <a class="btn btn-secondary" href="{{route('surat.index.admin')}}">Cancel</a>
            <button class="btn btn-primary" type="submit">Buat</button>
        </form>
    </div>
@endsection